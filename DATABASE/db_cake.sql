-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2019 at 04:17 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_cake`
--

-- --------------------------------------------------------

--
-- Table structure for table `halamanstatis`
--

CREATE TABLE `halamanstatis` (
  `id_halaman` int(5) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `judul_seo` varchar(100) NOT NULL,
  `isi_halaman` text NOT NULL,
  `tgl_posting` date NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `dibaca` int(5) NOT NULL DEFAULT '1',
  `jam` time NOT NULL,
  `hari` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `halamanstatis`
--

INSERT INTO `halamanstatis` (`id_halaman`, `judul`, `judul_seo`, `isi_halaman`, `tgl_posting`, `gambar`, `username`, `dibaca`, `jam`, `hari`) VALUES
(49, 'Tentang Kami', 'tentang-kami', '<p style="text-align:justify">Hallo sahabat yang luar biasa, pada kesempatan kali ini kai dari Toko Astri Cake untuk memperkenalkan diri. Mudah-mudahan dengan adanya perkenalan ini sahabat-sahabat sekalian akan lebih senang berbelabja di Toko kami. Sungguh terhomat bagi kami, jika anda datang ke Toko kami dan bisa memperoleh banyak hal yang bermanfaat. Astri Cake merupakan salah satu contoh usaha bisnis yang berjalan di bagian pemesanan dan penjualan berbagai macam kue ulang tahun yang berdiri pada tahun 2013. Nama pemilik dari Toko Astri Cake ini adalah Astri. Astri cake ini terletak di Jalan Sungai Raya Dalam Komplek Sejahtera 1 No A24 Pontianak dan mempunyai karyawan sekitar 4 orang. Usaha ini berawal dari pemilik yang menjual burger dan berbagai macam kue termasuk kue ulang tahun,karena kue ulang tahun banyak diminati konsumen maka kemudian dikembangkan menjadi usaha kue ulang tahun dan sampai saat ini masih terus dikembangkan dalam bidang pemasaran yang ditawarkan. Dengan adanya dari toko online ini diharapkan dapat mempermudah anda untuk mendapatkan berbagai produk berkualitas tinggi namun dengan harga yang terjangkau. Dalam memberi layanan, kami selalu mencoba memberi persembahan terbaik kepada siapapun. Selain itu kami juga selalu menjunjung tinggi nilai-nilai etika yang baik, seperti kejujuran, ketepatan, dan profesional dalam berbisnis. Mudah-mudahan dengannya toko online kami&nbsp; bisa memberi banyak manfaat bagi anda. Sekian perkenalan ini . Semoga perkenalan ini bisa memberi inspirasi dan manfaat bagi kita semua.&nbsp;</p>\r\n\r\n<p style="text-align:justify">&nbsp;</p>\r\n\r\n<p style="text-align:justify">Salam super dan semoga sukses selalu</p>\r\n\r\n<p style="text-align:justify">&nbsp;</p>\r\n\r\n<p style="text-align:justify">Astri</p>\r\n\r\n<p style="text-align:justify">&nbsp;</p>\r\n', '2019-08-04', 'IMG-20190804-WA00251.jpg', 'admin', 4, '19:28:14', ''),
(50, 'Cara Belanja', 'cara-belanja', '<p>Selamat datang di Toko Astri Cake. Demi kenyamanan anda berbelanja, berikut ini kami jelaskan tata cara berbelanja di Toko Astri Cake.</p>\r\n\r\n<p><strong>1. Memilih Produk</strong></p>\r\n\r\n<p>Silahkan melihat produk-produk yang tersedia di Toku Astri Cake melalui menu <strong>Semua Produk</strong>. Pada produk yang diinginkan, klik tombol <strong>Beli Sekarang</strong>&nbsp;untuk memasukkannya ke dalam keranjang belanjaan anda. Tambahkan produk lain sesuai keperluan.</p>\r\n\r\n<p><strong>2. Melihat Keranjang Belanja</strong></p>\r\n\r\n<p>Dibagian kanan&nbsp;atas&nbsp;terlihat menu <strong>Keranjang Belanja</strong>&nbsp; yang menampilkan jumlah item dalam belanjaan dan&nbsp;total nilai pesanan anda. Bila anda masih ingin berbelanja lagi maka klik <strong>Lanjut Belanja </strong>jika produk yang dipilih sudah sesuai keinginan anda&nbsp;klik<strong>&nbsp;</strong><strong>Selesai Belanja</strong>&nbsp;.</p>\r\n\r\n<p><strong>3. Membuat Akun</strong></p>\r\n\r\n<p>Anda akan diarahkan pada bagian <strong>Login</strong>. Bila anda sudah terdaftar, silahkan gunakan username&nbsp;dan password anda untuk login. Bila anda belum terdaftar, silahkan lakukan pendaftaran terlebih dahulu dengan meng-klik tombol <strong>Belum Punya akun</strong>. Bagi anda yang baru melakukan pendaftaran silhkan melengkapi beberapa data yang jelas. Yang perlu diperhatikan adalah pada saat menuliskan alamat. Jika sudah selesai maka klik <strong>Daftar.</strong></p>\r\n\r\n<p><strong>4. Cara Pengiriman&nbsp;</strong></p>\r\n\r\n<p>Pada bagian ini anda dapat memilih jasa pengiriman yaitu menggunakan jasa kurir (JNE, POS, TIKI) atau ambil sendiri. Jika anda memilih pengirimannya dengan menggunakan jasa kurir maka jumlah total harga belanjaan anda akan otomatis dihitung dengan ongkos kirimnya sesuai dengan alamat dan juga berat dari pesanan. jika sudah kemudian klik <strong>Lakukan Pembayaran</strong>.</p>\r\n\r\n<p><strong>5.&nbsp;Cara Pembayaran&nbsp;</strong></p>\r\n\r\n<p>Pada bagian ini anda mendaptkan nomor innvoice yaitu nomor transaksi anda. Mohon untuk disimpan nomor transaksinya untuk digunakan selanjutnya. Anda diarahkan untuk melakukan pembayaran. Kami saat ini hanya menerima pembayaran melalui transfer dengan nomor rekening yang sudah kami sediakan yaitu melalui BRI dan BCA.</p>\r\n\r\n<p><strong>6. Konfirmasi Pembayaran</strong></p>\r\n\r\n<p>Pada bagian ini jika anda sudah melakukan transfer pembayaran melalui nomor rekening yang sudah disediakan maka anda diarahkan untuk meng-upload bukti transfer pembayarannya agar kami dapat segera memproses pemesanan&nbsp;anda.&nbsp;Setelah itu kemudian klik <strong>Kirimkan</strong>.</p>\r\n\r\n<p><strong>7.Tracking Order</strong></p>\r\n\r\n<p>Pada bagian ini jika anda sudah melakukan transfer pembayaran melalui nomor rekening yang sudah disediakan Di menu tracking order ini anda bisa memasukkan nomor innvoice atau nomor transaksi anda yang sudah di dapatkan sebelumnya jika ingin melihat status pemesanan anda apakah sudah di konfirmasi oleh Toko Astri Cake atau belum. Jika anda sudah melakukan pembayaran maka status anda sedang di <strong>Proses </strong>jika&nbsp;belum melakukan pembayaran maka status anda <strong>Pending.</strong></p>\r\n\r\n<p><strong>8. Pengiriman dan Tracking Pengiriman</strong></p>\r\n\r\n<p>Apabila pengiriman telah kami lakukan, status pesanan anda akan diupdate.</p>\r\n', '2019-08-04', '', 'admin', 4, '14:08:50', ''),
(66, 'Halaman test', 'halaman-test', '<p>test halaman</p>\r\n', '2019-04-02', 'avatar1.png', 'admin', 2, '17:30:20', '');

-- --------------------------------------------------------

--
-- Table structure for table `hubungi`
--

CREATE TABLE `hubungi` (
  `id_hubungi` int(5) NOT NULL,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `ip_pengirim` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `subjek` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `pesan` text COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `dibaca` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `hubungi`
--

INSERT INTO `hubungi` (`id_hubungi`, `nama`, `ip_pengirim`, `email`, `subjek`, `pesan`, `tanggal`, `jam`, `dibaca`) VALUES
(57, 'Cahayu Anselamuti', '::1', 'cahayu.anselamuti88@gmail.com', 'Penilaian', 'Mantappp. Semoga sukses', '2019-08-04', '14:12:13', 'Y'),
(59, 'Irvan Albarta', '::1', 'irvan@gmail.com', 'Masukan', 'Lebih di percantik lagi hiasannya', '2019-08-04', '14:19:15', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `identitas`
--

CREATE TABLE `identitas` (
  `id_identitas` int(5) NOT NULL,
  `nama_website` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `facebook` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `rekening` varchar(100) NOT NULL,
  `no_telp` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `kota_id` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `meta_deskripsi` varchar(250) NOT NULL,
  `meta_keyword` varchar(250) NOT NULL,
  `favicon` varchar(50) NOT NULL,
  `maps` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `identitas`
--

INSERT INTO `identitas` (`id_identitas`, `nama_website`, `email`, `url`, `facebook`, `rekening`, `no_telp`, `kota_id`, `alamat`, `meta_deskripsi`, `meta_keyword`, `favicon`, `maps`) VALUES
(1, 'Astri Cake', 'astricake@gmail.co.id', 'http://localhost/astri_cake', 'Astri Cake', '937325246432453', '085750281659', 364, 'Jl. Sungai Raya Dalam Kompleks Sejahtera 1 A 24, Pontianak Kalimantan Barat', 'Menyajikan produk terbaik, terenak, tercepat, dan terpercaya.', 'Keyword', 'google.ico', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.8142842811144!2d109.34873681475327!3d-0.08149669994053545!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e1d5a3c57d92b7f%3A0x6ff3b38efb9e3bf8!2sKomp.+Sejahtera+1%2C+Jl.+Sungai+Raya+Dalam%2C+Sungai+Raya%2C+Kec.+Sungai+Raya%2C+Kabupaten+Kubu+Raya%2C+Kalimantan+Barat+78116!5e0!3m2!1sid!2sid!4v1563921026506!5m2!1sid!2sid\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" width=\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"700\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" height=\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"450\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" frameborder=\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"0\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" style=\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"border:0\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" allowfullscreen');

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

CREATE TABLE `logo` (
  `id_logo` int(5) NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`id_logo`, `gambar`) VALUES
(15, '2.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(5) NOT NULL,
  `id_parent` int(5) NOT NULL DEFAULT '0',
  `nama_menu` varchar(30) NOT NULL,
  `link` varchar(100) NOT NULL,
  `aktif` enum('Ya','Tidak') NOT NULL DEFAULT 'Ya',
  `position` enum('Top','Bottom') DEFAULT 'Bottom',
  `urutan` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `id_parent`, `nama_menu`, `link`, `aktif`, `position`, `urutan`) VALUES
(143, 0, 'Konfirmasi Pembayaran', 'konfirmasi', 'Ya', 'Bottom', 24),
(113, 0, 'Tentang Kami', 'page/detail/tentang-kami', 'Ya', 'Bottom', 4),
(7, 0, 'Beranda', '', 'Ya', 'Bottom', 1),
(136, 0, 'Login', 'auth/login', 'Ya', 'Top', 15),
(137, 0, 'Register', 'auth/register', 'Ya', 'Top', 16),
(138, 0, 'Kontak Kami', 'contact', 'Ya', 'Bottom', 3),
(142, 0, 'Testimoni', 'testimoni', 'Ya', 'Bottom', 22),
(144, 0, 'Semua Produk', 'produk', 'Ya', 'Bottom', 2),
(145, 146, 'Cara Belanja', 'page/detail/cara-belanja', 'Ya', 'Bottom', 2),
(146, 0, 'Informasi', '#', 'Ya', 'Bottom', 2),
(147, 0, 'Tracking Order', 'konfirmasi/tracking', 'Ya', 'Bottom', 23);

-- --------------------------------------------------------

--
-- Table structure for table `pasangiklan`
--

CREATE TABLE `pasangiklan` (
  `id_pasangiklan` int(5) NOT NULL,
  `judul` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `pasangiklan`
--

INSERT INTO `pasangiklan` (`id_pasangiklan`, `judul`, `username`, `url`, `gambar`, `tgl_posting`) VALUES
(32, 'Temukan juga kami di Tokopedia', 'admin', 'https://www.tokopedia.com', 'tokopedia1.png', '2018-03-04'),
(33, 'Temukan juga kami di Bukalapak', 'admin', 'https://www.bukalapak.com', 'download.png', '2018-03-04');

-- --------------------------------------------------------

--
-- Table structure for table `rb_kategori_produk`
--

CREATE TABLE `rb_kategori_produk` (
  `id_kategori_produk` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL,
  `kategori_seo` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rb_kategori_produk`
--

INSERT INTO `rb_kategori_produk` (`id_kategori_produk`, `nama_kategori`, `kategori_seo`) VALUES
(21, 'Kue Resepsi Pernikahan', 'kue-resepsi-pernikahan'),
(22, 'Kue Ulang Tahun', 'kue-ulang-tahun');

-- --------------------------------------------------------

--
-- Table structure for table `rb_konfirmasi`
--

CREATE TABLE `rb_konfirmasi` (
  `id_konfirmasi_pembayaran` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `total_transfer` varchar(20) NOT NULL,
  `id_rekening` int(11) NOT NULL,
  `nama_pengirim` varchar(255) NOT NULL,
  `tanggal_transfer` date NOT NULL,
  `bukti_transfer` varchar(255) NOT NULL,
  `waktu_konfirmasi` datetime NOT NULL,
  `konfirmasi_dibaca` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rb_konfirmasi`
--

INSERT INTO `rb_konfirmasi` (`id_konfirmasi_pembayaran`, `id_penjualan`, `total_transfer`, `id_rekening`, `nama_pengirim`, `tanggal_transfer`, `bukti_transfer`, `waktu_konfirmasi`, `konfirmasi_dibaca`) VALUES
(17, 34, 'Rp 70,000', 1, 'riyan', '2019-08-01', 'IMG20190416212442.jpg', '2019-08-01 13:47:35', 'Y'),
(18, 36, 'Rp 240,000', 2, 'Cahayu Anselamuti', '2019-05-04', 'IMG-20190523-WA0027.jpg', '2019-08-04 15:26:18', 'Y'),
(19, 38, 'Rp 1,300,000', 2, 'Irvan Albarta', '2019-05-14', 'WhatsApp_Image_2019-08-04_at_14_23_09.jpeg', '2019-08-05 21:33:18', 'Y'),
(20, 39, 'Rp 174,000', 1, 'Irvan Albarta', '2019-02-23', 'WhatsApp_Image_2019-08-04_at_14_23_091.jpeg', '2019-08-05 21:40:13', 'Y'),
(21, 40, 'Rp 300,000', 1, 'Irvan Albarta', '2019-03-02', 'WhatsApp_Image_2019-08-04_at_14_23_092.jpeg', '2019-08-05 21:42:54', 'Y'),
(22, 41, 'Rp 300,000', 2, 'Cahayu Anselamuti', '2019-04-17', 'WhatsApp_Image_2019-08-04_at_14_23_093.jpeg', '2019-08-05 21:49:00', 'N'),
(23, 42, 'Rp 610,000', 1, 'Cahayu Anselamuti', '2019-06-24', 'WhatsApp_Image_2019-08-04_at_14_23_094.jpeg', '2019-08-05 21:51:36', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `rb_konsumen`
--

CREATE TABLE `rb_konsumen` (
  `id_konsumen` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` text NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `email` varchar(60) NOT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `alamat_lengkap` text NOT NULL,
  `kota_id` int(11) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `tanggal_daftar` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rb_konsumen`
--

INSERT INTO `rb_konsumen` (`id_konsumen`, `username`, `password`, `nama_lengkap`, `email`, `jenis_kelamin`, `tanggal_lahir`, `tempat_lahir`, `alamat_lengkap`, `kota_id`, `no_hp`, `foto`, `tanggal_daftar`) VALUES
(37, 'cahayu', '7a674153c63cff1ad7f0e261c369ab2c', 'Cahayu Anselamuti', 'cahayu.anselamuti88@gmail.com', 'Perempuan', '1998-09-14', 'Pontianak', 'Jalan Sungai Raya Dalam . Komplek Srikandi No.2', 365, '085751093200', '', '2019-08-04'),
(35, 'riyan', '202cb962ac59075b964b07152d234b70', 'Riyan Andrian', 'riyan@gmail.com', 'Laki-laki', '1998-08-12', 'Jakarta', 'Jalan Perdana No.24', 364, '085267895413', '', '2019-07-28'),
(36, 'irvan', 'bcbe3365e6ac95ea2c0343a2395834dd', 'Irvan Albarta', 'irvan22@gmail.com', 'Laki-laki', '1998-02-22', 'Pontianak', 'Jalan Media', 365, '0823678976', '', '2019-08-02');

-- --------------------------------------------------------

--
-- Table structure for table `rb_kota`
--

CREATE TABLE `rb_kota` (
  `kota_id` int(11) NOT NULL,
  `provinsi_id` int(11) NOT NULL,
  `nama_kota` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rb_kota`
--

INSERT INTO `rb_kota` (`kota_id`, `provinsi_id`, `nama_kota`) VALUES
(17, 1, 'Badung'),
(32, 1, 'Bangli'),
(94, 1, 'Buleleng'),
(114, 1, 'Denpasar'),
(128, 1, 'Gianyar'),
(161, 1, 'Jembrana'),
(170, 1, 'Karangasem'),
(197, 1, 'Klungkung'),
(447, 1, 'Tabanan'),
(27, 2, 'Bangka'),
(28, 2, 'Bangka Barat'),
(29, 2, 'Bangka Selatan'),
(30, 2, 'Bangka Tengah'),
(56, 2, 'Belitung'),
(57, 2, 'Belitung Timur'),
(334, 2, 'Pangkal Pinang'),
(106, 3, 'Cilegon'),
(232, 3, 'Lebak'),
(331, 3, 'Pandeglang'),
(402, 3, 'Serang'),
(403, 3, 'Serang'),
(455, 3, 'Tangerang'),
(456, 3, 'Tangerang'),
(457, 3, 'Tangerang Selatan'),
(62, 4, 'Bengkulu'),
(63, 4, 'Bengkulu Selatan'),
(64, 4, 'Bengkulu Tengah'),
(65, 4, 'Bengkulu Utara'),
(175, 4, 'Kaur'),
(183, 4, 'Kepahiang'),
(233, 4, 'Lebong'),
(294, 4, 'Muko Muko'),
(379, 4, 'Rejang Lebong'),
(397, 4, 'Seluma'),
(39, 5, 'Bantul'),
(135, 5, 'Gunung Kidul'),
(210, 5, 'Kulon Progo'),
(419, 5, 'Sleman'),
(501, 5, 'Yogyakarta'),
(151, 6, 'Jakarta Barat'),
(152, 6, 'Jakarta Pusat'),
(153, 6, 'Jakarta Selatan'),
(154, 6, 'Jakarta Timur'),
(155, 6, 'Jakarta Utara'),
(189, 6, 'Kepulauan Seribu'),
(77, 7, 'Boalemo'),
(88, 7, 'Bone Bolango'),
(129, 7, 'Gorontalo'),
(130, 7, 'Gorontalo'),
(131, 7, 'Gorontalo Utara'),
(361, 7, 'Pohuwato'),
(50, 8, 'Batang Hari'),
(97, 8, 'Bungo'),
(156, 8, 'Jambi'),
(194, 8, 'Kerinci'),
(280, 8, 'Merangin'),
(293, 8, 'Muaro Jambi'),
(393, 8, 'Sarolangun'),
(442, 8, 'Sungaipenuh'),
(460, 8, 'Tanjung Jabung Barat'),
(461, 8, 'Tanjung Jabung Timur'),
(471, 8, 'Tebo'),
(22, 9, 'Bandung'),
(23, 9, 'Bandung'),
(24, 9, 'Bandung Barat'),
(34, 9, 'Banjar'),
(54, 9, 'Bekasi'),
(55, 9, 'Bekasi'),
(78, 9, 'Bogor'),
(79, 9, 'Bogor'),
(103, 9, 'Ciamis'),
(104, 9, 'Cianjur'),
(107, 9, 'Cimahi'),
(108, 9, 'Cirebon'),
(109, 9, 'Cirebon'),
(115, 9, 'Depok'),
(126, 9, 'Garut'),
(149, 9, 'Indramayu'),
(171, 9, 'Karawang'),
(211, 9, 'Kuningan'),
(252, 9, 'Majalengka'),
(332, 9, 'Pangandaran'),
(376, 9, 'Purwakarta'),
(428, 9, 'Subang'),
(430, 9, 'Sukabumi'),
(431, 9, 'Sukabumi'),
(440, 9, 'Sumedang'),
(468, 9, 'Tasikmalaya'),
(469, 9, 'Tasikmalaya'),
(37, 10, 'Banjarnegara'),
(41, 10, 'Banyumas'),
(49, 10, 'Batang'),
(76, 10, 'Blora'),
(91, 10, 'Boyolali'),
(92, 10, 'Brebes'),
(105, 10, 'Cilacap'),
(113, 10, 'Demak'),
(134, 10, 'Grobogan'),
(163, 10, 'Jepara'),
(169, 10, 'Karanganyar'),
(177, 10, 'Kebumen'),
(181, 10, 'Kendal'),
(196, 10, 'Klaten'),
(209, 10, 'Kudus'),
(249, 10, 'Magelang'),
(250, 10, 'Magelang'),
(344, 10, 'Pati'),
(348, 10, 'Pekalongan'),
(349, 10, 'Pekalongan'),
(352, 10, 'Pemalang'),
(375, 10, 'Purbalingga'),
(377, 10, 'Purworejo'),
(380, 10, 'Rembang'),
(386, 10, 'Salatiga'),
(398, 10, 'Semarang'),
(399, 10, 'Semarang'),
(427, 10, 'Sragen'),
(433, 10, 'Sukoharjo'),
(445, 10, 'Surakarta (Solo)'),
(472, 10, 'Tegal'),
(473, 10, 'Tegal'),
(476, 10, 'Temanggung'),
(497, 10, 'Wonogiri'),
(498, 10, 'Wonosobo'),
(31, 11, 'Bangkalan'),
(42, 11, 'Banyuwangi'),
(51, 11, 'Batu'),
(74, 11, 'Blitar'),
(75, 11, 'Blitar'),
(80, 11, 'Bojonegoro'),
(86, 11, 'Bondowoso'),
(133, 11, 'Gresik'),
(160, 11, 'Jember'),
(164, 11, 'Jombang'),
(178, 11, 'Kediri'),
(179, 11, 'Kediri'),
(222, 11, 'Lamongan'),
(243, 11, 'Lumajang'),
(247, 11, 'Madiun'),
(248, 11, 'Madiun'),
(251, 11, 'Magetan'),
(256, 11, 'Malang'),
(255, 11, 'Malang'),
(289, 11, 'Mojokerto'),
(290, 11, 'Mojokerto'),
(305, 11, 'Nganjuk'),
(306, 11, 'Ngawi'),
(317, 11, 'Pacitan'),
(330, 11, 'Pamekasan'),
(342, 11, 'Pasuruan'),
(343, 11, 'Pasuruan'),
(363, 11, 'Ponorogo'),
(369, 11, 'Probolinggo'),
(370, 11, 'Probolinggo'),
(390, 11, 'Sampang'),
(409, 11, 'Sidoarjo'),
(418, 11, 'Situbondo'),
(441, 11, 'Sumenep'),
(444, 11, 'Surabaya'),
(487, 11, 'Trenggalek'),
(489, 11, 'Tuban'),
(492, 11, 'Tulungagung'),
(61, 12, 'Bengkayang'),
(168, 12, 'Kapuas Hulu'),
(176, 12, 'Kayong Utara'),
(195, 12, 'Ketapang'),
(208, 12, 'Kubu Raya'),
(228, 12, 'Landak'),
(279, 12, 'Melawi'),
(364, 12, 'Pontianak'),
(365, 12, 'Pontianak'),
(388, 12, 'Sambas'),
(391, 12, 'Sanggau'),
(395, 12, 'Sekadau'),
(415, 12, 'Singkawang'),
(417, 12, 'Sintang'),
(18, 13, 'Balangan'),
(33, 13, 'Banjar'),
(35, 13, 'Banjarbaru'),
(36, 13, 'Banjarmasin'),
(43, 13, 'Barito Kuala'),
(143, 13, 'Hulu Sungai Selatan'),
(144, 13, 'Hulu Sungai Tengah'),
(145, 13, 'Hulu Sungai Utara'),
(203, 13, 'Kotabaru'),
(446, 13, 'Tabalong'),
(452, 13, 'Tanah Bumbu'),
(454, 13, 'Tanah Laut'),
(466, 13, 'Tapin'),
(44, 14, 'Barito Selatan'),
(45, 14, 'Barito Timur'),
(46, 14, 'Barito Utara'),
(136, 14, 'Gunung Mas'),
(167, 14, 'Kapuas'),
(174, 14, 'Katingan'),
(205, 14, 'Kotawaringin Barat'),
(206, 14, 'Kotawaringin Timur'),
(221, 14, 'Lamandau'),
(296, 14, 'Murung Raya'),
(326, 14, 'Palangka Raya'),
(371, 14, 'Pulang Pisau'),
(405, 14, 'Seruyan'),
(432, 14, 'Sukamara'),
(19, 15, 'Balikpapan'),
(66, 15, 'Berau'),
(89, 15, 'Bontang'),
(214, 15, 'Kutai Barat'),
(215, 15, 'Kutai Kartanegara'),
(216, 15, 'Kutai Timur'),
(341, 15, 'Paser'),
(354, 15, 'Penajam Paser Utara'),
(387, 15, 'Samarinda'),
(96, 16, 'Bulungan (Bulongan)'),
(257, 16, 'Malinau'),
(311, 16, 'Nunukan'),
(450, 16, 'Tana Tidung'),
(467, 16, 'Tarakan'),
(48, 17, 'Batam'),
(71, 17, 'Bintan'),
(172, 17, 'Karimun'),
(184, 17, 'Kepulauan Anambas'),
(237, 17, 'Lingga'),
(302, 17, 'Natuna'),
(462, 17, 'Tanjung Pinang'),
(21, 18, 'Bandar Lampung'),
(223, 18, 'Lampung Barat'),
(224, 18, 'Lampung Selatan'),
(225, 18, 'Lampung Tengah'),
(226, 18, 'Lampung Timur'),
(227, 18, 'Lampung Utara'),
(282, 18, 'Mesuji'),
(283, 18, 'Metro'),
(355, 18, 'Pesawaran'),
(356, 18, 'Pesisir Barat'),
(368, 18, 'Pringsewu'),
(458, 18, 'Tanggamus'),
(490, 18, 'Tulang Bawang'),
(491, 18, 'Tulang Bawang Barat'),
(496, 18, 'Way Kanan'),
(14, 19, 'Ambon'),
(99, 19, 'Buru'),
(100, 19, 'Buru Selatan'),
(185, 19, 'Kepulauan Aru'),
(258, 19, 'Maluku Barat Daya'),
(259, 19, 'Maluku Tengah'),
(260, 19, 'Maluku Tenggara'),
(261, 19, 'Maluku Tenggara Barat'),
(400, 19, 'Seram Bagian Barat'),
(401, 19, 'Seram Bagian Timur'),
(488, 19, 'Tual'),
(138, 20, 'Halmahera Barat'),
(139, 20, 'Halmahera Selatan'),
(140, 20, 'Halmahera Tengah'),
(141, 20, 'Halmahera Timur'),
(142, 20, 'Halmahera Utara'),
(191, 20, 'Kepulauan Sula'),
(372, 20, 'Pulau Morotai'),
(477, 20, 'Ternate'),
(478, 20, 'Tidore Kepulauan'),
(1, 21, 'Aceh Barat'),
(2, 21, 'Aceh Barat Daya'),
(3, 21, 'Aceh Besar'),
(4, 21, 'Aceh Jaya'),
(5, 21, 'Aceh Selatan'),
(6, 21, 'Aceh Singkil'),
(7, 21, 'Aceh Tamiang'),
(8, 21, 'Aceh Tengah'),
(9, 21, 'Aceh Tenggara'),
(10, 21, 'Aceh Timur'),
(11, 21, 'Aceh Utara'),
(20, 21, 'Banda Aceh'),
(59, 21, 'Bener Meriah'),
(72, 21, 'Bireuen'),
(127, 21, 'Gayo Lues'),
(230, 21, 'Langsa'),
(235, 21, 'Lhokseumawe'),
(300, 21, 'Nagan Raya'),
(358, 21, 'Pidie'),
(359, 21, 'Pidie Jaya'),
(384, 21, 'Sabang'),
(414, 21, 'Simeulue'),
(429, 21, 'Subulussalam'),
(68, 22, 'Bima'),
(69, 22, 'Bima'),
(118, 22, 'Dompu'),
(238, 22, 'Lombok Barat'),
(239, 22, 'Lombok Tengah'),
(240, 22, 'Lombok Timur'),
(241, 22, 'Lombok Utara'),
(276, 22, 'Mataram'),
(438, 22, 'Sumbawa'),
(439, 22, 'Sumbawa Barat'),
(13, 23, 'Alor'),
(58, 23, 'Belu'),
(122, 23, 'Ende'),
(125, 23, 'Flores Timur'),
(212, 23, 'Kupang'),
(213, 23, 'Kupang'),
(234, 23, 'Lembata'),
(269, 23, 'Manggarai'),
(270, 23, 'Manggarai Barat'),
(271, 23, 'Manggarai Timur'),
(301, 23, 'Nagekeo'),
(304, 23, 'Ngada'),
(383, 23, 'Rote Ndao'),
(385, 23, 'Sabu Raijua'),
(412, 23, 'Sikka'),
(434, 23, 'Sumba Barat'),
(435, 23, 'Sumba Barat Daya'),
(436, 23, 'Sumba Tengah'),
(437, 23, 'Sumba Timur'),
(479, 23, 'Timor Tengah Selatan'),
(480, 23, 'Timor Tengah Utara'),
(16, 24, 'Asmat'),
(67, 24, 'Biak Numfor'),
(90, 24, 'Boven Digoel'),
(111, 24, 'Deiyai (Deliyai)'),
(117, 24, 'Dogiyai'),
(150, 24, 'Intan Jaya'),
(157, 24, 'Jayapura'),
(158, 24, 'Jayapura'),
(159, 24, 'Jayawijaya'),
(180, 24, 'Keerom'),
(193, 24, 'Kepulauan Yapen (Yapen Waropen)'),
(231, 24, 'Lanny Jaya'),
(263, 24, 'Mamberamo Raya'),
(264, 24, 'Mamberamo Tengah'),
(274, 24, 'Mappi'),
(281, 24, 'Merauke'),
(284, 24, 'Mimika'),
(299, 24, 'Nabire'),
(303, 24, 'Nduga'),
(335, 24, 'Paniai'),
(347, 24, 'Pegunungan Bintang'),
(373, 24, 'Puncak'),
(374, 24, 'Puncak Jaya'),
(392, 24, 'Sarmi'),
(443, 24, 'Supiori'),
(484, 24, 'Tolikara'),
(495, 24, 'Waropen'),
(499, 24, 'Yahukimo'),
(500, 24, 'Yalimo'),
(124, 25, 'Fakfak'),
(165, 25, 'Kaimana'),
(272, 25, 'Manokwari'),
(273, 25, 'Manokwari Selatan'),
(277, 25, 'Maybrat'),
(346, 25, 'Pegunungan Arfak'),
(378, 25, 'Raja Ampat'),
(424, 25, 'Sorong'),
(425, 25, 'Sorong'),
(426, 25, 'Sorong Selatan'),
(449, 25, 'Tambrauw'),
(474, 25, 'Teluk Bintuni'),
(475, 25, 'Teluk Wondama'),
(60, 26, 'Bengkalis'),
(120, 26, 'Dumai'),
(147, 26, 'Indragiri Hilir'),
(148, 26, 'Indragiri Hulu'),
(166, 26, 'Kampar'),
(187, 26, 'Kepulauan Meranti'),
(207, 26, 'Kuantan Singingi'),
(350, 26, 'Pekanbaru'),
(351, 26, 'Pelalawan'),
(381, 26, 'Rokan Hilir'),
(382, 26, 'Rokan Hulu'),
(406, 26, 'Siak'),
(253, 27, 'Majene'),
(262, 27, 'Mamasa'),
(265, 27, 'Mamuju'),
(266, 27, 'Mamuju Utara'),
(362, 27, 'Polewali Mandar'),
(38, 28, 'Bantaeng'),
(47, 28, 'Barru'),
(87, 28, 'Bone'),
(95, 28, 'Bulukumba'),
(123, 28, 'Enrekang'),
(132, 28, 'Gowa'),
(162, 28, 'Jeneponto'),
(244, 28, 'Luwu'),
(245, 28, 'Luwu Timur'),
(246, 28, 'Luwu Utara'),
(254, 28, 'Makassar'),
(275, 28, 'Maros'),
(328, 28, 'Palopo'),
(333, 28, 'Pangkajene Kepulauan'),
(336, 28, 'Parepare'),
(360, 28, 'Pinrang'),
(396, 28, 'Selayar (Kepulauan Selayar)'),
(408, 28, 'Sidenreng Rappang/Rapang'),
(416, 28, 'Sinjai'),
(423, 28, 'Soppeng'),
(448, 28, 'Takalar'),
(451, 28, 'Tana Toraja'),
(486, 28, 'Toraja Utara'),
(493, 28, 'Wajo'),
(25, 29, 'Banggai'),
(26, 29, 'Banggai Kepulauan'),
(98, 29, 'Buol'),
(119, 29, 'Donggala'),
(291, 29, 'Morowali'),
(329, 29, 'Palu'),
(338, 29, 'Parigi Moutong'),
(366, 29, 'Poso'),
(410, 29, 'Sigi'),
(482, 29, 'Tojo Una-Una'),
(483, 29, 'Toli-Toli'),
(53, 30, 'Bau-Bau'),
(85, 30, 'Bombana'),
(101, 30, 'Buton'),
(102, 30, 'Buton Utara'),
(182, 30, 'Kendari'),
(198, 30, 'Kolaka'),
(199, 30, 'Kolaka Utara'),
(200, 30, 'Konawe'),
(201, 30, 'Konawe Selatan'),
(202, 30, 'Konawe Utara'),
(295, 30, 'Muna'),
(494, 30, 'Wakatobi'),
(73, 31, 'Bitung'),
(81, 31, 'Bolaang Mongondow (Bolmong)'),
(82, 31, 'Bolaang Mongondow Selatan'),
(83, 31, 'Bolaang Mongondow Timur'),
(84, 31, 'Bolaang Mongondow Utara'),
(188, 31, 'Kepulauan Sangihe'),
(190, 31, 'Kepulauan Siau Tagulandang Biaro (Sitaro)'),
(192, 31, 'Kepulauan Talaud'),
(204, 31, 'Kotamobagu'),
(267, 31, 'Manado'),
(285, 31, 'Minahasa'),
(286, 31, 'Minahasa Selatan'),
(287, 31, 'Minahasa Tenggara'),
(288, 31, 'Minahasa Utara'),
(485, 31, 'Tomohon'),
(12, 32, 'Agam'),
(93, 32, 'Bukittinggi'),
(116, 32, 'Dharmasraya'),
(186, 32, 'Kepulauan Mentawai'),
(236, 32, 'Lima Puluh Koto/Kota'),
(318, 32, 'Padang'),
(321, 32, 'Padang Panjang'),
(322, 32, 'Padang Pariaman'),
(337, 32, 'Pariaman'),
(339, 32, 'Pasaman'),
(340, 32, 'Pasaman Barat'),
(345, 32, 'Payakumbuh'),
(357, 32, 'Pesisir Selatan'),
(394, 32, 'Sawah Lunto'),
(411, 32, 'Sijunjung (Sawah Lunto Sijunjung)'),
(420, 32, 'Solok'),
(421, 32, 'Solok'),
(422, 32, 'Solok Selatan'),
(453, 32, 'Tanah Datar'),
(40, 33, 'Banyuasin'),
(121, 33, 'Empat Lawang'),
(220, 33, 'Lahat'),
(242, 33, 'Lubuk Linggau'),
(292, 33, 'Muara Enim'),
(297, 33, 'Musi Banyuasin'),
(298, 33, 'Musi Rawas'),
(312, 33, 'Ogan Ilir'),
(313, 33, 'Ogan Komering Ilir'),
(314, 33, 'Ogan Komering Ulu'),
(315, 33, 'Ogan Komering Ulu Selatan'),
(316, 33, 'Ogan Komering Ulu Timur'),
(324, 33, 'Pagar Alam'),
(327, 33, 'Palembang'),
(367, 33, 'Prabumulih'),
(15, 34, 'Asahan'),
(52, 34, 'Batu Bara'),
(70, 34, 'Binjai'),
(110, 34, 'Dairi'),
(112, 34, 'Deli Serdang'),
(137, 34, 'Gunungsitoli'),
(146, 34, 'Humbang Hasundutan'),
(173, 34, 'Karo'),
(217, 34, 'Labuhan Batu'),
(218, 34, 'Labuhan Batu Selatan'),
(219, 34, 'Labuhan Batu Utara'),
(229, 34, 'Langkat'),
(268, 34, 'Mandailing Natal'),
(278, 34, 'Medan'),
(307, 34, 'Nias'),
(308, 34, 'Nias Barat'),
(309, 34, 'Nias Selatan'),
(310, 34, 'Nias Utara'),
(319, 34, 'Padang Lawas'),
(320, 34, 'Padang Lawas Utara'),
(323, 34, 'Padang Sidempuan'),
(325, 34, 'Pakpak Bharat'),
(353, 34, 'Pematang Siantar'),
(389, 34, 'Samosir'),
(404, 34, 'Serdang Bedagai'),
(407, 34, 'Sibolga'),
(413, 34, 'Simalungun'),
(459, 34, 'Tanjung Balai'),
(463, 34, 'Tapanuli Selatan'),
(464, 34, 'Tapanuli Tengah'),
(465, 34, 'Tapanuli Utara'),
(470, 34, 'Tebing Tinggi'),
(481, 34, 'Toba Samosir');

-- --------------------------------------------------------

--
-- Table structure for table `rb_penjualan`
--

CREATE TABLE `rb_penjualan` (
  `id_penjualan` int(11) NOT NULL,
  `kode_transaksi` varchar(50) NOT NULL,
  `id_konsumen` int(11) NOT NULL,
  `diskon` int(11) NOT NULL,
  `kurir` varchar(255) DEFAULT NULL,
  `service` varchar(255) DEFAULT NULL,
  `ongkir` int(11) NOT NULL,
  `waktu_transaksi` datetime NOT NULL,
  `proses` enum('0','1','2','3') NOT NULL,
  `penjualan_dibaca` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rb_penjualan`
--

INSERT INTO `rb_penjualan` (`id_penjualan`, `kode_transaksi`, `id_konsumen`, `diskon`, `kurir`, `service`, `ongkir`, `waktu_transaksi`, `proses`, `penjualan_dibaca`) VALUES
(42, 'TRX-20190805215019', 37, 0, 'ambil sendiri', '*', 0, '2019-08-05 21:50:53', '2', 'N'),
(41, 'TRX-20190805214658', 37, 0, 'ambil sendiri', '*', 0, '2019-08-05 21:48:03', '2', 'N'),
(40, 'TRX-20190805214207', 36, 0, 'ambil sendiri', '*', 0, '2019-08-05 21:42:15', '1', 'Y'),
(39, 'TRX-20190805213720', 36, 0, 'jne', 'YES', 24000, '2019-08-05 21:38:14', '1', 'Y'),
(38, 'TRX-20190805213029', 36, 0, 'ambil sendiri', '*', 0, '2019-08-05 21:30:51', '1', 'Y'),
(37, 'TRX-20190804193407', 37, 0, 'jne', 'REG', 27000, '2019-08-04 19:42:32', '1', 'Y'),
(35, 'TRX-20190804122708', 36, 0, 'jne', 'OKE', 8000, '2019-08-04 12:53:23', '3', 'Y'),
(36, 'TRX-20190804152346', 37, 0, 'ambil sendiri', '*', 0, '2019-08-04 15:24:36', '2', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `rb_penjualan_detail`
--

CREATE TABLE `rb_penjualan_detail` (
  `id_penjualan_detail` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `satuan` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rb_penjualan_detail`
--

INSERT INTO `rb_penjualan_detail` (`id_penjualan_detail`, `id_penjualan`, `id_produk`, `jumlah`, `harga_jual`, `satuan`) VALUES
(40, 34, 28, 1, 70000, 'pcs'),
(39, 33, 25, 900, 60000, 'pcs'),
(38, 32, 25, 1, 60000, 'pcs'),
(37, 31, 25, 10, 60000, 'pcs'),
(36, 30, 25, 76, 60000, 'pcs'),
(35, 29, 25, 90, 60000, 'pcs'),
(34, 28, 25, 40, 60000, 'pcs'),
(41, 35, 29, 1, 120000, 'pcs'),
(42, 36, 38, 2, 120000, 'pcs'),
(43, 37, 37, 1, 300000, 'pcs'),
(44, 38, 41, 1, 1300000, 'pcs'),
(45, 39, 46, 1, 150000, 'pcs'),
(46, 40, 37, 1, 300000, 'pcs'),
(47, 41, 46, 1, 150000, 'pcs'),
(48, 41, 45, 1, 150000, 'pcs'),
(49, 42, 36, 1, 185000, 'pcs'),
(50, 42, 44, 1, 185000, 'pcs'),
(51, 42, 29, 1, 120000, 'pcs'),
(52, 42, 38, 1, 120000, 'pcs');

--
-- Triggers `rb_penjualan_detail`
--
DELIMITER $$
CREATE TRIGGER `TG_STOK` AFTER INSERT ON `rb_penjualan_detail` FOR EACH ROW BEGIN
	UPDATE rb_produk SET stok=stok-NEW.jumlah
    WHERE id_produk=NEW.id_produk;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `rb_penjualan_temp`
--

CREATE TABLE `rb_penjualan_temp` (
  `id_penjualan_detail` int(11) NOT NULL,
  `session` varchar(50) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `satuan` varchar(50) NOT NULL,
  `waktu_order` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rb_penjualan_temp`
--

INSERT INTO `rb_penjualan_temp` (`id_penjualan_detail`, `session`, `id_produk`, `jumlah`, `harga_jual`, `satuan`, `waktu_order`) VALUES
(92, 'TRX-20190731140022', 25, 2, 60000, 'pcs', '2019-07-31 14:00:22'),
(93, 'TRX-20190731140022', 28, 1, 70000, 'pcs', '2019-07-31 14:07:39'),
(94, 'TRX-20190801182016', 28, 1, 70000, 'pcs', '2019-08-01 18:20:16'),
(98, 'TRX-20190804211100', 33, 1, 300000, 'pcs', '2019-08-04 21:11:00'),
(109, 'TRX-20190818211602', 30, 5, 135000, 'pcs', '2019-08-18 21:16:02');

-- --------------------------------------------------------

--
-- Table structure for table `rb_produk`
--

CREATE TABLE `rb_produk` (
  `id_produk` int(11) NOT NULL,
  `id_kategori_produk` int(11) NOT NULL,
  `nama_produk` varchar(255) NOT NULL,
  `produk_seo` varchar(255) NOT NULL,
  `satuan` varchar(50) NOT NULL,
  `harga_konsumen` int(11) NOT NULL,
  `berat` varchar(50) NOT NULL,
  `stok` int(10) NOT NULL,
  `diskon` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `waktu_input` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rb_produk`
--

INSERT INTO `rb_produk` (`id_produk`, `id_kategori_produk`, `nama_produk`, `produk_seo`, `satuan`, `harga_konsumen`, `berat`, `stok`, `diskon`, `gambar`, `keterangan`, `username`, `waktu_input`) VALUES
(30, 22, 'Kue Ulang Tahun Karakter Tayo', 'kue-ulang-tahun-karakter-tayo', 'pcs', 135000, '1000', 10, 0, '81.jpeg', '<p>Ukuran 18 cm</p>\r\n', 'admin', '2019-08-04 14:49:00'),
(31, 22, 'Kue Ulang Tahun Karakter Sofia', 'kue-ulang-tahun-karakter-sofia', 'pcs', 200000, '2000', 0, 0, '61.jpeg', '<p>Ukuran 22 cm</p>\r\n', 'admin', '2019-08-04 14:50:56'),
(29, 22, 'Kue Ulang Tahun Karakter Doraemon', 'kue-ulang-tahun-karakter-doraemon', 'pcs', 120000, '1000', 0, 0, '71.jpeg', '', 'admin', '2019-08-04 12:15:20'),
(32, 22, 'Kue Ulang Tahun Karakter Helo Kitty', 'kue-ulang-tahun-karakter-helo-kitty', 'pcs', 170000, '2000', 0, 0, '31.jpeg', '<p>Ukuran 20 cm</p>\r\n', 'admin', '2019-08-04 14:52:09'),
(33, 22, 'Kue Ulang Tahun Karakter Kuda Pony', 'kue-ulang-tahun-karakter-kuda-pony', 'pcs', 300000, '3000', 0, 0, '151.jpeg', '<p>Ukuran 24 cm&nbsp;</p>\r\n\r\n<p>Sudah full hias</p>\r\n', 'admin', '2019-08-04 14:55:00'),
(34, 22, 'Kue Ulang Tahun Love Coklat', 'kue-ulang-tahun-love-coklat', 'pcs', 65000, '1000', 0, 0, '21.jpeg', '<p>Ukuran 18 cm</p>\r\n', 'admin', '2019-08-04 14:58:25'),
(35, 22, 'Kue Ulang Tahun Bulat Cherry', 'kue-ulang-tahun-bulat-cherry', 'pcs', 165000, '1000', 0, 0, '121.jpeg', '<p>Ukuran 20 cm</p>\r\n', 'admin', '2019-08-04 15:00:20'),
(36, 22, 'Kue Ulang Tahun Karakter Bunga Matahari', 'kue-ulang-tahun-karakter-bunga-matahari', 'pcs', 185000, '2000', 0, 0, '141.jpeg', '<p>Ukuran 22 cm</p>\r\n', 'admin', '2019-08-04 15:01:43'),
(37, 22, 'Kue Ulang Tahun Bunga Orange', 'kue-ulang-tahun-bunga-orange', 'pcs', 300000, '3000', 0, 0, '161.jpeg', '', 'admin', '2019-08-04 15:03:24'),
(38, 22, 'Kue Ulang Tahun Coklat toping buah', 'kue-ulang-tahun-coklat-toping-buah', 'pcs', 120000, '1000', 0, 0, '181.jpeg', '<p>Ukuran 18 cm</p>\r\n', 'admin', '2019-08-04 15:05:13'),
(39, 22, 'Kue Ulang Tahun Karakter Foto toping buah', 'kue-ulang-tahun-karakter-foto-toping-buah', 'pcs', 230000, '2000', 0, 0, '111.jpeg', '<p>Ukuran 22 cm</p>\r\n', 'admin', '2019-08-04 15:07:44'),
(40, 22, 'Kue Ulang Tahun Bulat Strowberry', 'kue-ulang-tahun-bulat-strowberry', 'pcs', 135000, '1000', 0, 0, '131.jpeg', '<p>Ukuran 20 cm</p>\r\n', 'admin', '2019-08-04 15:09:01'),
(41, 21, 'Kue Wedding Party', 'kue-wedding-party', 'pcs', 1300000, '10000', 0, 0, 'WhatsApp_Image_2019-07-23_at_20_41_17.jpeg', '', 'admin', '2019-08-05 21:15:55'),
(42, 21, 'Kue Wedding Party', 'kue-wedding-party', 'pcs', 1200000, '10000', 0, 0, 'WhatsApp_Image_2019-07-26_at_07_28_14.jpeg', '', 'admin', '2019-08-05 21:17:07'),
(43, 21, 'Kue Wedding Party', 'kue-wedding-party', 'pcs', 1300000, '10000', 0, 0, 'WhatsApp_Image_2019-07-26_at_07_37_56.jpeg', '', 'admin', '2019-08-05 21:18:11'),
(44, 22, 'Kue Ulang Tahun Karakter Turtles Ninja', 'kue-ulang-tahun-karakter-turtles-ninja', 'pcs', 185000, '2000', 0, 0, 'WhatsApp_Image_2019-07-28_at_21_15_09.jpeg', '<p>Ukuran 22 cm</p>\r\n\r\n<p>Kunya aja tanpa miniatur</p>\r\n', 'admin', '2019-08-05 21:23:07'),
(45, 22, 'Kue Ulang Tahun Karakter Angry Birds', 'kue-ulang-tahun-karakter-angry-birds', 'pcs', 150000, '1000', 0, 0, 'WhatsApp_Image_2019-07-28_at_21_14_26.jpeg', '<p>Ukuran 20 cm</p>\r\n', 'admin', '2019-08-05 21:24:11'),
(46, 22, 'Kue Ulang Tahun ', 'kue-ulang-tahun-', 'pcs', 150000, '2000', 0, 0, 'WhatsApp_Image_2019-07-28_at_21_13_57.jpeg', '<p>Ukuran 20 cm</p>\r\n', 'admin', '2019-08-05 21:26:02');

-- --------------------------------------------------------

--
-- Table structure for table `rb_provinsi`
--

CREATE TABLE `rb_provinsi` (
  `provinsi_id` int(11) NOT NULL,
  `nama_provinsi` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rb_provinsi`
--

INSERT INTO `rb_provinsi` (`provinsi_id`, `nama_provinsi`) VALUES
(1, 'Bali'),
(2, 'Bangka Belitung'),
(3, 'Banten'),
(4, 'Bengkulu'),
(5, 'DI Yogyakarta'),
(6, 'DKI Jakarta'),
(7, 'Gorontalo'),
(8, 'Jambi'),
(9, 'Jawa Barat'),
(10, 'Jawa Tengah'),
(11, 'Jawa Timur'),
(12, 'Kalimantan Barat'),
(13, 'Kalimantan Selatan'),
(14, 'Kalimantan Tengah'),
(15, 'Kalimantan Timur'),
(16, 'Kalimantan Utara'),
(17, 'Kepulauan Riau'),
(18, 'Lampung'),
(19, 'Maluku'),
(20, 'Maluku Utara'),
(21, 'Nanggroe Aceh Darussalam (NAD)'),
(22, 'Nusa Tenggara Barat (NTB)'),
(23, 'Nusa Tenggara Timur (NTT)'),
(24, 'Papua'),
(25, 'Papua Barat'),
(26, 'Riau'),
(27, 'Sulawesi Barat'),
(28, 'Sulawesi Selatan'),
(29, 'Sulawesi Tengah'),
(30, 'Sulawesi Tenggara'),
(31, 'Sulawesi Utara'),
(32, 'Sumatera Barat'),
(33, 'Sumatera Selatan'),
(34, 'Sumatera Utara');

-- --------------------------------------------------------

--
-- Table structure for table `rb_rekening`
--

CREATE TABLE `rb_rekening` (
  `id_rekening` int(5) NOT NULL,
  `nama_bank` varchar(50) NOT NULL,
  `no_rekening` varchar(50) NOT NULL,
  `pemilik_rekening` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rb_rekening`
--

INSERT INTO `rb_rekening` (`id_rekening`, `nama_bank`, `no_rekening`, `pemilik_rekening`) VALUES
(1, 'BRI', '382659239532692', 'ASTRI CAKE'),
(2, 'BCA', '9378542134', 'ASTRI CAKE');

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE `slide` (
  `id_slide` int(5) NOT NULL,
  `keterangan` text NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide`
--

INSERT INTO `slide` (`id_slide`, `keterangan`, `gambar`, `waktu`) VALUES
(12, 'Slide 1', 'IMG-20190804-WA00241.jpg', '2019-03-25 11:07:49'),
(13, 'Slide 2', 'IMG-20190804-WA00231.jpg', '2019-03-25 11:08:26'),
(14, 'Slide 3', 'IMG-20190804-WA00251.jpg', '2019-03-25 11:08:38');

-- --------------------------------------------------------

--
-- Table structure for table `statistik`
--

CREATE TABLE `statistik` (
  `ip` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `hits` int(10) NOT NULL DEFAULT '1',
  `online` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statistik`
--

INSERT INTO `statistik` (`ip`, `tanggal`, `hits`, `online`) VALUES
('::1', '2019-03-25', 195, '1553532623'),
('::1', '2019-03-27', 381, '1553705999'),
('::1', '2019-03-28', 182, '1553789747'),
('::1', '2019-03-29', 584, '1553878316'),
('::1', '2019-03-30', 529, '1553952970'),
('::1', '2019-03-31', 38, '1554044906'),
('::1', '2019-04-02', 98, '1554202531'),
('::1', '2019-04-11', 28, '1554963944'),
('::1', '2019-04-17', 1, '1555478572'),
('::1', '2019-04-18', 1, '1555593123'),
('::1', '2019-07-21', 176, '1563719599'),
('::1', '2019-07-23', 20, '1563879832'),
('::1', '2019-07-24', 78, '1563980888'),
('::1', '2019-07-28', 261, '1564322867'),
('::1', '2019-07-31', 33, '1564557107'),
('::1', '2019-08-01', 59, '1564659283'),
('::1', '2019-08-02', 10, '1564698356'),
('::1', '2019-08-03', 6, '1564823761'),
('::1', '2019-08-04', 228, '1564927874'),
('::1', '2019-08-05', 126, '1565016697'),
('::1', '2019-08-06', 5, '1565086732'),
('::1', '2019-08-18', 24, '1566137828');

-- --------------------------------------------------------

--
-- Table structure for table `table_admin`
--

CREATE TABLE `table_admin` (
  `id_users` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `level` varchar(20) NOT NULL DEFAULT 'user',
  `id_session` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_admin`
--

INSERT INTO `table_admin` (`id_users`, `username`, `password`, `nama_lengkap`, `email`, `no_telp`, `foto`, `level`, `id_session`) VALUES
(1, 'admin', '3e39b3844837bdefc8017fbcb386ea302af877fb17baa09d0a1bd34b67bbf2b34fba314bbcab450f5f3f73771b7aea956ba3320defda029723f4fdff7dfa007b', 'Administrator', 'admin@admin.co.id', '081578451278', 'avatar2.png', 'admin', '6760a34757f766bb28a622dba041d412');

-- --------------------------------------------------------

--
-- Table structure for table `testimoni`
--

CREATE TABLE `testimoni` (
  `id_testimoni` int(11) NOT NULL,
  `id_konsumen` int(11) NOT NULL,
  `isi_testimoni` text NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  `waktu_testimoni` datetime NOT NULL,
  `testimoni_dibaca` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimoni`
--

INSERT INTO `testimoni` (`id_testimoni`, `id_konsumen`, `isi_testimoni`, `aktif`, `waktu_testimoni`, `testimoni_dibaca`) VALUES
(15, 33, '<p>toko nya bagus, respond nya cepat</p>\r\n', 'Y', '2019-04-02 17:43:31', 'N'),
(13, 32, '<p>test</p>\r\n', 'Y', '2019-03-27 21:52:20', 'Y'),
(18, 36, '<p>Kuenya enak dan cream nya juga gak terlalu manis</p>\r\n', 'Y', '2019-08-04 13:53:24', 'N'),
(19, 35, '<p>Kak enak bangettt kuenya.Anak ku suka banget deh. Aku nanti pesan lagi yaaa</p>\r\n', 'Y', '2019-08-04 13:59:25', 'N'),
(20, 37, '<p>Kak nanti saya pesan lagi ya kuenya. Orang rumah saya pada suka semua. Kuenya enak bangetttt</p>\r\n', 'Y', '2019-08-04 14:03:12', 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `halamanstatis`
--
ALTER TABLE `halamanstatis`
  ADD PRIMARY KEY (`id_halaman`);

--
-- Indexes for table `hubungi`
--
ALTER TABLE `hubungi`
  ADD PRIMARY KEY (`id_hubungi`);

--
-- Indexes for table `identitas`
--
ALTER TABLE `identitas`
  ADD PRIMARY KEY (`id_identitas`);

--
-- Indexes for table `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id_logo`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `pasangiklan`
--
ALTER TABLE `pasangiklan`
  ADD PRIMARY KEY (`id_pasangiklan`);

--
-- Indexes for table `rb_kategori_produk`
--
ALTER TABLE `rb_kategori_produk`
  ADD PRIMARY KEY (`id_kategori_produk`);

--
-- Indexes for table `rb_konfirmasi`
--
ALTER TABLE `rb_konfirmasi`
  ADD PRIMARY KEY (`id_konfirmasi_pembayaran`);

--
-- Indexes for table `rb_konsumen`
--
ALTER TABLE `rb_konsumen`
  ADD PRIMARY KEY (`id_konsumen`);

--
-- Indexes for table `rb_kota`
--
ALTER TABLE `rb_kota`
  ADD PRIMARY KEY (`kota_id`);

--
-- Indexes for table `rb_penjualan`
--
ALTER TABLE `rb_penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `rb_penjualan_detail`
--
ALTER TABLE `rb_penjualan_detail`
  ADD PRIMARY KEY (`id_penjualan_detail`);

--
-- Indexes for table `rb_penjualan_temp`
--
ALTER TABLE `rb_penjualan_temp`
  ADD PRIMARY KEY (`id_penjualan_detail`);

--
-- Indexes for table `rb_produk`
--
ALTER TABLE `rb_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `rb_provinsi`
--
ALTER TABLE `rb_provinsi`
  ADD PRIMARY KEY (`provinsi_id`);

--
-- Indexes for table `rb_rekening`
--
ALTER TABLE `rb_rekening`
  ADD PRIMARY KEY (`id_rekening`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id_slide`);

--
-- Indexes for table `table_admin`
--
ALTER TABLE `table_admin`
  ADD PRIMARY KEY (`id_users`);

--
-- Indexes for table `testimoni`
--
ALTER TABLE `testimoni`
  ADD PRIMARY KEY (`id_testimoni`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `halamanstatis`
--
ALTER TABLE `halamanstatis`
  MODIFY `id_halaman` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `hubungi`
--
ALTER TABLE `hubungi`
  MODIFY `id_hubungi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `identitas`
--
ALTER TABLE `identitas`
  MODIFY `id_identitas` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `logo`
--
ALTER TABLE `logo`
  MODIFY `id_logo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `pasangiklan`
--
ALTER TABLE `pasangiklan`
  MODIFY `id_pasangiklan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `rb_kategori_produk`
--
ALTER TABLE `rb_kategori_produk`
  MODIFY `id_kategori_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `rb_konfirmasi`
--
ALTER TABLE `rb_konfirmasi`
  MODIFY `id_konfirmasi_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `rb_konsumen`
--
ALTER TABLE `rb_konsumen`
  MODIFY `id_konsumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `rb_kota`
--
ALTER TABLE `rb_kota`
  MODIFY `kota_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;
--
-- AUTO_INCREMENT for table `rb_penjualan`
--
ALTER TABLE `rb_penjualan`
  MODIFY `id_penjualan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `rb_penjualan_detail`
--
ALTER TABLE `rb_penjualan_detail`
  MODIFY `id_penjualan_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `rb_penjualan_temp`
--
ALTER TABLE `rb_penjualan_temp`
  MODIFY `id_penjualan_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `rb_produk`
--
ALTER TABLE `rb_produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `rb_provinsi`
--
ALTER TABLE `rb_provinsi`
  MODIFY `provinsi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `rb_rekening`
--
ALTER TABLE `rb_rekening`
  MODIFY `id_rekening` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `id_slide` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `table_admin`
--
ALTER TABLE `table_admin`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `testimoni`
--
ALTER TABLE `testimoni`
  MODIFY `id_testimoni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
