<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Administrator extends CI_Controller {
	function index(){
		
		if (isset($_POST['submit'])){
			$username = $this->input->post('a');
			$password = hash("sha512", md5($this->input->post('b')));
			// $password = md5($this->input->post('b'));
			$cek = $this->db->query("SELECT * FROM table_admin where username='".$this->db->escape_str($username)."' AND password='".$this->db->escape_str($password)."'");
			$row = $cek->row_array();
			$total = $cek->num_rows();
			if ($total > 0){
				$this->session->set_userdata('upload_image_file_manager',true);
				$this->session->set_userdata(array('id_users'=>$row['id_users'],
					'username'=>$row['username'],
					'level'=>$row['level'],
					'id_session'=>$row['id_session']));
				redirect('administrator/home');
			}else{
				$data['title'] = 'Administrator &rsaquo; Astri Cake';
				$this->load->view('administrator/view_login',$data);
			}
		}else{
			$data['title'] = 'Administrator &rsaquo; Astri Cake';
			$this->load->view('administrator/view_login',$data);
		}
		
	}

		// Controller Modul Download

	function download(){
		cek_session_akses('download',$this->session->id_session);
		$data['record'] = $this->model_download->index();
		$this->template->load('administrator/template','administrator/mod_download/view_download',$data);
	}

	function tambah_download(){
		cek_session_akses('download',$this->session->id_session);
		if (isset($_POST['submit'])){
			$this->model_download->download_tambah();
			redirect('administrator/download');
		}else{
			$this->template->load('administrator/template','administrator/mod_download/view_download_tambah');
		}
	}

	function edit_download(){
		cek_session_akses('download',$this->session->id_session);
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$this->model_download->download_update();
			redirect('administrator/download');
		}else{
			$data['rows'] = $this->model_download->download_edit($id)->row_array();
			$this->template->load('administrator/template','administrator/mod_download/view_download_edit',$data);
		}
	}

	function delete_download(){
		cek_session_akses('download',$this->session->id_session);
		$id = $this->uri->segment(3);
		$this->model_download->download_delete($id);
		redirect('administrator/download');
	}

	public function download_file(){
		$name = $this->uri->segment(3);
		$data = file_get_contents("assets/bukti_transfer/".$name);
		force_download($name, $data);
	}


	function home(){
		if ($this->session->id_session){
			$this->template->load('administrator/template','administrator/view_home');
		}else{
			redirect('administrator');
		}
	}

  // Controller Modul Iklan Sidebar

	function iklansidebar(){
		if ($this->session->id_session){
			$data['record'] = $this->model_iklan->iklan_sidebar();
			$this->template->load('administrator/template','administrator/mod_iklansidebar/view_iklansidebar',$data);
		} else {
			redirect('administrator');
		}
	}

	function tambah_iklansidebar(){
		if ($this->session->id_session){
			if (isset($_POST['submit'])){
				$this->model_iklan->iklan_sidebar_tambah();
				redirect('administrator/iklansidebar');
			}else{
				$this->template->load('administrator/template','administrator/mod_iklansidebar/view_iklansidebar_tambah');
			}
		} else {
			redirect('administrator');
		}
	}

	function edit_iklansidebar(){
		if ($this->session->id_session){
			$id = $this->uri->segment(3);
			if (isset($_POST['submit'])){
				$this->model_iklan->iklan_sidebar_update();
				redirect('administrator/iklansidebar');
			}else{
				$data['rows'] = $this->model_iklan->iklan_sidebar_edit($id)->row_array();
				$this->template->load('administrator/template','administrator/mod_iklansidebar/view_iklansidebar_edit',$data);
			}
		} else {
			redirect('administrator');
		}
	}

	function delete_iklansidebar(){
		if ($this->session->id_session){
			$id = $this->uri->segment(3);
			$this->model_iklan->iklan_sidebar_delete($id);
			redirect('administrator/iklansidebar');
		}else {
			redirect('administrator');
		}
	}

	function laporan_harian(){
		if ($this->session->id_session){
			$data = $this->model_app->hari_ini();
			$data = array('record' => $data);
			$this->template->load('administrator/template','administrator/mod_laporan/view_harian',$data);
		}else{
			redirect('administrator');
		}	
	}

	function laporan_mingguan(){
		if ($this->session->id_session){
			$data = $this->model_app->minggu_ini();
			$data = array('record' => $data);
			$this->template->load('administrator/template','administrator/mod_laporan/view_mingguan',$data);
		} else {
			redirect('administrator');
		}
	}

	function laporan_bulanan(){
		if ($this->session->id_session){
			if(isset($_POST['submit'])){
				$bulan = $this->input->post('bulan');
				$data = $this->model_app->bulan($bulan);
				$data = array('record' => $data);
				$this->template->load('administrator/template','administrator/mod_laporan/view_bulanan',$data);
			} else {
				$data = $this->model_app->bulan_ini();
				$data = array('record' => $data);
				$this->template->load('administrator/template','administrator/mod_laporan/view_bulanan',$data);
			}

		} else {
			redirect('administrator');
		}

	}

	//Controller Manajemen User
	function edit_manajemenuser(){
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$config['upload_path'] = 'asset/foto_user/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '1000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('f');
            $hasil=$this->upload->data();
            if ($hasil['file_name']=='' AND $this->input->post('b') ==''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')));
            }elseif ($hasil['file_name']!='' AND $this->input->post('b') ==''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'foto'=>$hasil['file_name']);
            }elseif ($hasil['file_name']=='' AND $this->input->post('b') !=''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'password'=>hash("sha512", md5($this->input->post('b'))),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')));
            }elseif ($hasil['file_name']!='' AND $this->input->post('b') !=''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                                    'password'=>hash("sha512", md5($this->input->post('b'))),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                                    'email'=>$this->db->escape_str($this->input->post('d')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                                    'foto'=>$hasil['file_name']);
            }
            $where = array('username' => $this->input->post('id'));
            $this->model_app->update('table_admin', $data, $where);

              $mod=count($this->input->post('modul'));
              $modul=$this->input->post('modul');
              for($i=0;$i<$mod;$i++){
                $datam = array('id_session'=>$this->input->post('ids'),
                              'id_modul'=>$modul[$i]);
                // $this->model_app->insert('users_modul',$datam);
              }

			redirect('administrator/edit_manajemenuser/'.$this->input->post('id'));
		}else{
            if ($this->session->username==$this->uri->segment(3) OR $this->session->level=='admin'){
                $proses = $this->model_app->edit('table_admin', array('username' => $id))->row_array();
                // $akses = $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
                // $modul = $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
                $data = array('rows' => $proses, 'record' => $modul);
    			$this->template->load('administrator/template','administrator/mod_users/view_users_edit',$data);
            }else{
                redirect('administrator/edit_manajemenuser/'.$this->session->username);
            }
		}
	}


  // Controller Modul Kategori Produk

	function kategori_produk(){
		if ($this->session->id_session){
			$data['record'] = $this->model_app->view_ordering('rb_kategori_produk','id_kategori_produk','DESC');
			$this->template->load('administrator/template','administrator/mod_kategori_produk/view_kategori_produk',$data);
		} else {
			redirect('administrator');
		}
	}

	function tambah_kategori_produk(){
		if ($this->session->id_session){
			if (isset($_POST['submit'])){
				$data = array('nama_kategori'=>$this->input->post('a'),'kategori_seo'=>seo_title($this->input->post('a')));
				$this->model_app->insert('rb_kategori_produk',$data);
				redirect('administrator/kategori_produk');
			}else{
				$this->template->load('administrator/template','administrator/mod_kategori_produk/view_kategori_produk_tambah');
			}
		} else {
			redirect('administrator');
		}
	}

	function edit_kategori_produk(){
		if ($this->session->id_session){
			$id = $this->uri->segment(3);
			if (isset($_POST['submit'])){
				$data = array('nama_kategori'=>$this->input->post('a'),'kategori_seo'=>seo_title($this->input->post('a')));
				$where = array('id_kategori_produk' => $this->input->post('id'));
				$this->model_app->update('rb_kategori_produk', $data, $where);
				redirect('administrator/kategori_produk');
			}else{
				$edit = $this->model_app->edit('rb_kategori_produk',array('id_kategori_produk'=>$id))->row_array();
				$data = array('rows' => $edit);
				$this->template->load('administrator/template','administrator/mod_kategori_produk/view_kategori_produk_edit',$data);
			}
		} else {
			redirect('administrator');
		}
	}

	function delete_kategori_produk(){
		if ($this->session->id_session){
			$id = array('id_kategori_produk' => $this->uri->segment(3));
			$this->model_app->delete('rb_kategori_produk',$id);
			redirect('administrator/kategori_produk');
		} else {
			redirect('administrator');
		}
	}


	// Controller Modul Produk

	function produk(){
		if ($this->session->id_session){
			$data['record'] = $this->model_app->view_ordering('rb_produk','id_produk','DESC');
			$this->template->load('administrator/template','administrator/mod_produk/view_produk',$data);
		} else {
			redirect('administrator');
		}
	}

	function tambah_produk(){
		if ($this->session->id_session){
			if (isset($_POST['submit'])){
				$config['upload_path'] = 'assets/foto_produk/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '5000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('g');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
            	$data = array('id_kategori_produk'=>$this->input->post('a'),
            		'nama_produk'=>$this->db->escape_str($this->input->post('b')),
            		'produk_seo'=>$this->db->escape_str(seo_title($this->input->post('b'))),
            		'satuan'=>$this->input->post('c'),
            		'harga_konsumen'=>$this->input->post('f'),
            		'berat'=>$this->input->post('berat'),
            		'stok'=>$this->input->post('stok'),
            		'keterangan'=>$this->input->post('ff'),
            		'username'=>$this->session->username,
            		'waktu_input'=>date('Y-m-d H:i:s'));
            }else{
            	$data = array('id_kategori_produk'=>$this->input->post('a'),
            		'nama_produk'=>$this->input->post('b'),
            		'produk_seo'=>$this->db->escape_str(seo_title($this->input->post('b'))),
            		'satuan'=>$this->input->post('c'),
            		'harga_konsumen'=>$this->input->post('f'),
            		'berat'=>$this->input->post('berat'),
            		'stok'=>$this->input->post('stok'),
            		'gambar'=>$hasil['file_name'],
            		'keterangan'=>$this->input->post('ff'),
            		'username'=>$this->session->username,
            		'waktu_input'=>date('Y-m-d H:i:s'));
            }
            $this->model_app->insert('rb_produk',$data);
            redirect('administrator/produk');
        }else{
        	$data['record'] = $this->model_app->view_ordering('rb_kategori_produk','id_kategori_produk','DESC');
        	$this->template->load('administrator/template','administrator/mod_produk/view_produk_tambah',$data);
        }
    } else {
    	redirect('administrator');
    }
}

function edit_produk(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$config['upload_path'] = 'assets/foto_produk/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '5000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('g');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
            	$data = array('id_kategori_produk'=>$this->input->post('a'),
            		'nama_produk'=>$this->input->post('b'),
            		'produk_seo'=>$this->db->escape_str(seo_title($this->input->post('b'))),
            		'satuan'=>$this->input->post('c'),
            		'harga_konsumen'=>$this->input->post('f'),
            		'berat'=>$this->input->post('berat'),
            		'stok'=>$this->input->post('stok'),
            		'keterangan'=>$this->input->post('ff'),
            		'username'=>$this->session->username);
            }else{
            	$data = array('id_kategori_produk'=>$this->input->post('a'),
            		'nama_produk'=>$this->input->post('b'),
            		'produk_seo'=>$this->db->escape_str(seo_title($this->input->post('b'))),
            		'satuan'=>$this->input->post('c'),
            		'harga_konsumen'=>$this->input->post('f'),
            		'berat'=>$this->input->post('berat'),
            		'stok'=>$this->input->post('stok'),
            		'gambar'=>$hasil['file_name'],
            		'keterangan'=>$this->input->post('ff'),
            		'username'=>$this->session->username);
            }
            $where = array('id_produk' => $this->input->post('id'));
            $this->model_app->update('rb_produk', $data, $where);
            redirect('administrator/produk');
        }else{
        	$data['record'] = $this->model_app->view_ordering('rb_kategori_produk','id_kategori_produk','DESC');
        	$data['rows'] = $this->model_app->edit('rb_produk',array('id_produk'=>$id))->row_array();
        	$this->template->load('administrator/template','administrator/mod_produk/view_produk_edit',$data);
        }
    } else {
    	redirect('administrator');
    }
}

function delete_produk(){
	if ($this->session->id_session){
		$id = array('id_produk' => $this->uri->segment(3));
		$this->model_app->delete('rb_produk',$id);
		redirect('administrator/produk');
	} else {
		redirect('administrator');
	}
}


  // Controller Modul Rekening

function rekening(){
	if ($this->session->id_session){
		$data['record'] = $this->model_rekening->rekening();
		$this->template->load('administrator/template','administrator/mod_rekening/view_rekening',$data);
	} else {
		redirect('administrator');
	}
}

function tambah_rekening(){
		// cek_session_akses('rekening',$this->session->id_session);
	if (isset($_POST['submit'])){
		$this->model_rekening->rekening_tambah();
		redirect('administrator/rekening');
	}else{
		$this->template->load('administrator/template','administrator/mod_rekening/view_rekening_tambah');
	}
}

function edit_rekening(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$this->model_rekening->rekening_update();
			redirect('administrator/rekening');
		}else{
			$data['rows'] = $this->model_rekening->rekening_edit($id)->row_array();
			$this->template->load('administrator/template','administrator/mod_rekening/view_rekening_edit',$data);
		}
	} else {
		redirect('administrator');
	}
}

function delete_rekening(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		$this->model_rekening->rekening_delete($id);
		redirect('administrator/rekening');
	} else {
		redirect('administrator');
	}
}


  // Controller Modul Konsumen

function konsumen(){
	if ($this->session->id_session){
		$data['record'] = $this->model_app->view_ordering('rb_konsumen','id_konsumen','DESC');
		$this->template->load('administrator/template','administrator/mod_konsumen/view_konsumen',$data);
	} else {
		redirect('administrator');
	}
}

function edit_konsumen(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$this->model_members->profile_update($this->input->post('id'));
			redirect('administrator/edit_konsumen/'.$this->input->post('id'));
		}else{
			$data['row'] = $this->model_app->profile_konsumen($id)->row_array();
			$data['kota'] = $this->model_app->view('rb_kota');
			$this->template->load('administrator/template','administrator/mod_konsumen/view_konsumen_edit',$data);
		}
	} else {
		redirect('administrator');
	}
}

function detail_konsumen(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		$record = $this->model_app->orders_report($id);
		$edit = $this->model_app->profile_konsumen($id)->row_array();
		$data = array('rows' => $edit,'record'=>$record);
		$this->template->load('administrator/template','administrator/mod_konsumen/view_konsumen_detail',$data);
	} else {
		redirect('administrator');
	}
}

function delete_konsumen(){
	if ($this->session->id_session){
		$id = array('id_konsumen' => $this->uri->segment(3));
		$this->model_app->delete('rb_konsumen',$id);
		redirect('administrator/konsumen');
	} else {
		redirect('administrator');
	} 
}


function orders(){
	if ($this->session->id_session){
		$data['title'] = 'Laporan Pesanan Masuk';
		$data['record'] = $this->model_app->orders_report_all();
		$this->template->load('administrator/template','administrator/mod_penjualan/view_orders_report',$data);
	} else {
		redirect('administrator');
	}
}

function konfirmasi(){
	if ($this->session->id_session){
		$data['title'] = 'Konfirmasi Pembayaran Pesanan';
		$data['record'] = $this->model_app->konfirmasi_bayar();
		$this->template->load('administrator/template','administrator/mod_penjualan/view_konfirmasi_bayar',$data);
	} else {
		redirect('administrator');
	}
}

function orders_status(){
	if ($this->session->id_session){
		$data = array('proses'=>$this->uri->segment(4));
		$where = array('id_penjualan' => $this->uri->segment(3));
		$this->model_app->update('rb_penjualan', $data, $where);
		redirect('administrator/orders');
	} else {
		redirect('administrator');
	}
}


function tracking(){
	if ($this->session->id_session){
		if ($this->uri->segment(3)!=''){
			$kode_transaksi = filter($this->uri->segment(3));
			$this->db->query("UPDATE rb_konfirmasi SET konfirmasi_dibaca='Y' where id_konfirmasi_pembayaran");
			$data['title'] = 'Tracking Order '.$kode_transaksi;
			$data['rows'] = $this->db->query("SELECT * FROM rb_penjualan a JOIN rb_konsumen b ON a.id_konsumen=b.id_konsumen JOIN rb_kota c ON b.kota_id=c.kota_id where a.kode_transaksi='$kode_transaksi'")->row_array();
			$data['record'] = $this->db->query("SELECT a.kode_transaksi, b.*, c.nama_produk, c.satuan, c.berat, c.diskon, c.produk_seo FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON b.id_produk=c.id_produk where a.kode_transaksi='".$kode_transaksi."'");
			$data['total'] = $this->db->query("SELECT a.id_penjualan, a.kode_transaksi, a.kurir, a.service, a.proses, a.ongkir, sum((b.harga_jual*b.jumlah)-(c.diskon*b.jumlah)) as total, sum(c.berat*b.jumlah) as total_berat FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON b.id_produk=c.id_produk where a.kode_transaksi='".$kode_transaksi."'")->row_array();
			$this->template->load('administrator/template','administrator/mod_penjualan/view_tracking',$data);
		}
	} else {
		redirect('administrator');
	}
}

function detail_penjualan(){
	if ($this->session->id_session){
		if ($this->uri->segment(3)!=''){
			$kode_transaksi = filter($this->uri->segment(3));
			$this->db->query("UPDATE rb_penjualan SET penjualan_dibaca='Y' where id_penjualan");
			$data['title'] = 'Tracking Order '.$kode_transaksi;
			$data['rows'] = $this->db->query("SELECT * FROM rb_penjualan a JOIN rb_konsumen b ON a.id_konsumen=b.id_konsumen JOIN rb_kota c ON b.kota_id=c.kota_id where a.kode_transaksi='$kode_transaksi'")->row_array();
			$data['record'] = $this->db->query("SELECT a.kode_transaksi, b.*, c.nama_produk, c.satuan, c.berat, c.diskon, c.produk_seo FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON b.id_produk=c.id_produk where a.kode_transaksi='".$kode_transaksi."'");
			$data['total'] = $this->db->query("SELECT a.id_penjualan, a.kode_transaksi, a.kurir, a.service, a.proses, a.ongkir, sum((b.harga_jual*b.jumlah)-(c.diskon*b.jumlah)) as total, sum(c.berat*b.jumlah) as total_berat FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON b.id_produk=c.id_produk where a.kode_transaksi='".$kode_transaksi."'")->row_array();
			$this->template->load('administrator/template','administrator/mod_penjualan/view_tracking',$data);
		}
	} else {
		redirect('administrator');
	}
}

  // Controller Modul Logo

function logowebsite(){
	if ($this->session->id_session){
		if (isset($_POST['submit'])){
			$this->model_main->logo_update();
			redirect('administrator/logowebsite');
		}else{
			$data['record'] = $this->model_main->logo();
			$this->template->load('administrator/template','administrator/mod_logowebsite/view_logowebsite',$data);
		}
	} else {
		redirect('administrator');
	} 
}

  // Controller Modul Testimoni

function testimoni(){
	if ($this->session->id_session){
		$data['record'] = $this->model_testimoni->testimoni();
		$this->template->load('administrator/template','administrator/mod_testimoni/view_testimoni',$data);
	} else {
		redirect('administrator');
	}
}

function edit_testimoni(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$this->model_testimoni->testimoni_update();
			redirect('administrator/testimoni');
		}else{
			$data['rows'] = $this->model_testimoni->testimoni_edit($id)->row_array();
			$this->template->load('administrator/template','administrator/mod_testimoni/view_testimoni_edit',$data);
		}
	} else {
		redirect('administrator');
	}
}

function delete_testimoni(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		$this->model_testimoni->testimoni_delete($id);
		redirect('administrator/testimoni');
	} else {
		redirect('administrator');
	}
}

  // Controller Modul Pesan Masuk

function pesanmasuk(){
	if ($this->session->id_session){
		$data['record'] = $this->model_main->pesan_masuk();
		$this->template->load('administrator/template','administrator/mod_pesanmasuk/view_pesanmasuk',$data);
	} else {
		redirect('administrator');
	}
}

function detail_pesanmasuk(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		$this->db->query("UPDATE hubungi SET dibaca='Y' where id_hubungi='$id'");
		if (isset($_POST['submit'])){
			$this->model_main->pesan_masuk_kirim();
			$data['rows'] = $this->model_main->pesan_masuk_view($id)->row_array();
			$this->template->load('administrator/template','administrator/mod_pesanmasuk/view_pesanmasuk_detail',$data);
		}else{
			$data['rows'] = $this->model_main->pesan_masuk_view($id)->row_array();
			$this->template->load('administrator/template','administrator/mod_pesanmasuk/view_pesanmasuk_detail',$data);
		}
	} else {
		redirect('administrator');
	}
}

function delete_pesanmasuk(){
	if ($this->session->id_session){
		$id = array('id_hubungi' => $this->uri->segment(3));
		$this->model_app->delete('hubungi',$id);
		redirect('administrator/pesanmasuk');
	} else {
		redirect('administrator');
	}
}


  // Controller Modul Menu Website

function menuwebsite(){
	if ($this->session->id_session){
		$data['record'] = $this->model_menu->menu_website();
		$this->template->load('administrator/template','administrator/mod_menu/view_menu',$data);
	} else {
		redirect('administrator');
	}
}

function tambah_menuwebsite(){
	if ($this->session->id_session){
		if (isset($_POST['submit'])){
			$this->model_menu->menu_website_tambah();
			redirect('administrator/menuwebsite');
		}else{
			$data['record'] = $this->model_menu->menu_utama();
			$this->template->load('administrator/template','administrator/mod_menu/view_menu_tambah',$data);
		}
	} else {
		redirect('administrator');
	}
}

function edit_menuwebsite(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$this->model_menu->menu_website_update();
			redirect('administrator/menuwebsite');
		}else{
			$data['record'] = $this->model_menu->menu_utama();
			$data['rows'] = $this->model_menu->menu_edit($id)->row_array();
			$this->template->load('administrator/template','administrator/mod_menu/view_menu_edit',$data);
		}
	} else {
		redirect('administrator');
	}
}

function delete_menuwebsite(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		$this->model_menu->menu_delete($id);
		redirect('administrator/menuwebsite');
	} else {
		redirect('administrator');
	}
}



  // Controller Modul Halaman Baru

function halamanbaru(){
	if ($this->session->id_session){
		$data['record'] = $this->model_halaman->halamanstatis();
		$this->template->load('administrator/template','administrator/mod_halaman/view_halaman',$data);
	} else {
		redirect('administrator');
	}
}

function tambah_halamanbaru(){
	if ($this->session->id_session){
		if (isset($_POST['submit'])){
			$this->model_halaman->halamanstatis_tambah();
			redirect('administrator/halamanbaru');
		}else{
			$this->template->load('administrator/template','administrator/mod_halaman/view_halaman_tambah');
		}
	} else {
		redirect('administrator');
	}
}

function edit_halamanbaru(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$this->model_halaman->halamanstatis_update();
			redirect('administrator/halamanbaru');
		}else{
			$data['rows'] = $this->model_halaman->halamanstatis_edit($id)->row_array();
			$this->template->load('administrator/template','administrator/mod_halaman/view_halaman_edit',$data);
		}
	} else {
		redirect('administrator');
	}
}

function delete_halamanbaru(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		$this->model_halaman->halamanstatis_delete($id);
		redirect('administrator/halamanbaru');
	} else {
		redirect('administrator');
	}
}


  // Controller Modul Image Slider

function imagesslider(){
	if ($this->session->id_session){
		$data['record'] = $this->model_main->slide();
		$this->template->load('administrator/template','administrator/mod_slider/view_slider',$data);
	} else {
		redirect('administrator');
	}
}

function tambah_imagesslider(){
	if ($this->session->id_session){
		if (isset($_POST['submit'])){
			$this->model_main->slide_tambah();
			redirect('administrator/imagesslider');
		}else{
			$this->template->load('administrator/template','administrator/mod_slider/view_slider_tambah');
		}
	} else {
		redirect('administrator');
	}
}

function edit_imagesslider(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$this->model_main->slide_update();
			redirect('administrator/imagesslider');
		}else{
			$data['rows'] = $this->model_main->slide_edit($id)->row_array();
			$this->template->load('administrator/template','administrator/mod_slider/view_slider_edit',$data);
		}
	} else {
		redirect('administrator');
	}
}

function delete_imagesslider(){
	if ($this->session->id_session){
		$id = $this->uri->segment(3);
		$this->model_main->slide_delete($id);
		redirect('administrator/imagesslider');
	} else {
		redirect('administrator');
	}
}


// Controller Modul Identitas Website

function identitaswebsite(){
	if ($this->session->id_session){
		if (isset($_POST['submit'])){
			$this->model_main->identitas_update();
			redirect('administrator/identitaswebsite');
		}else{
			$data['record'] = $this->model_main->identitas()->row_array();
			$this->template->load('administrator/template','administrator/mod_identitas/view_identitas',$data);
		}
	} else {
		redirect('administrator');
	}
}


//Controller Model Logout

function logout(){
	$this->session->sess_destroy();
	redirect('main');
}

}
