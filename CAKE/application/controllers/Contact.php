<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
	public function index(){
		if (isset($_POST['submit'])){
			$this->model_main->kirim_Pesan();
			echo "<script>alert('Terima kasih! Pesan anda telah terkirim');history.go(-1);</script>";
		}else{
			$mp = $this->db->query("SELECT * FROM identitas")->row();
			$data['title'] = 'Hubungi Kami';
			$data['maps'] = $mp->maps;
			$this->template->load('cake/template','cake/view_contact',$data);
		}
	}
}
