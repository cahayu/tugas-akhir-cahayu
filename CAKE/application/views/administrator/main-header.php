<style type="text/css">
  .sekolah{
    float: left;
    background-color: transparent;
    background-image: none;
    padding: 15px 15px;
    font-family: fontAwesome;
    color:#fff;
  }

  .sekolah:hover{
    color:#fff;
  }
</style>
        <!-- Logo -->
        <a href="home" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>ADMINISTRATOR</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope"></i> Pesan Masuk
                  <?php $jmlh = $this->db->query("SELECT * FROM hubungi where dibaca='N'")->num_rows(); ?>
                  <span class="label label-success"><?php echo $jmlh; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Kamu punya <?php echo $jmlh; ?> pesan baru</li>
                  <li>
                    <ul class="menu">
                      <?php
                        $pesan = $this->model_main->pesan_baru(10);
                        foreach ($pesan->result_array() as $row) {
                          $isi_pesan = substr($row['pesan'],0,30);
                          $waktukirim = cek_terakhir($row['tanggal'].' '.$row['jam']);
                          if ($row['dibaca']=='N'){ $color = '#f4f4f4'; }else{ $color = '#fff'; }
                          echo "<li style='background-color:$color'>
                                  <a href='".base_url()."administrator/detail_pesanmasuk/$row[id_hubungi]'>
                                    <div class='pull-left'>
                                      <img src='".base_url()."assets/foto_user/blank.png' class='img-circle' alt='Image User'>
                                    </div>
                                    <h4>$row[nama]<small><i class='fa fa-clock-o'></i> $waktukirim</small></h4>
                                    <p>$isi_pesan...</p>
                                  </a>
                                </li>";
                        }
                      ?>
                    </ul>
                  </li>
                  <li class="footer"><a href="<?php echo base_url() ?>/administrator/pesanmasuk">Lihat semua pesan</a></li>
                </ul>
              </li>
              <li><a target='_BLANK' href="<?php echo base_url(); ?>"><i class="glyphicon glyphicon-new-window"></i></a></li>
            </ul>
          </div>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-camera"></i> Konfirmasi Pembayaran
                  <?php $jmlh = $this->db->query("SELECT * FROM rb_konfirmasi where konfirmasi_dibaca='N'")->num_rows(); ?>
                  <span class="label label-success"><?php echo $jmlh; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Kamu punya <?php echo $jmlh; ?> konfirmasi pembayaran baru</li>
                  <li>
                    <ul class="menu">
                      <?php
                      //cek foto konsumen
                        $konfirmasi = $this->model_main->konfirmasi_baru(10);
                        foreach ($konfirmasi->result_array() as $row) {
                          if (trim($row['foto'])==''){$foto_user = 'blank.png';} else {$foto_user = $row['foto']; }
                          $pengirim = substr($row['nama_pengirim'],0,30);
                          $waktukirim = cek_terakhir($row['waktu_konfirmasi']);
                          if ($row['konfirmasi_dibaca']=='N'){ $color = '#f4f4f4'; }else{ $color = '#fff'; }
                          echo "<li style='background-color:$color'>
                                  <a href='".base_url()."administrator/tracking/$row[kode_transaksi]'>
                                    <div class='pull-left'>
                                    <img src='".base_url()."assets/foto_user/$foto_user' class='img-circle' alt='Image User'>
                                    </div>
                                    <h4>$row[kode_transaksi]<small><i class='fa fa-clock-o'></i> $waktukirim</small></h4>
                                    <p>Pengirim : $pengirim </p>
                                  </a>
                                </li>";
                        }

                      ?>
                    </ul>
                  </li>
                  <li class="footer"><a href="<?php echo base_url() ?>/administrator/konfirmasi">Lihat semua konfirmasi pembayaran</a></li>
                </ul>
              </li>

            </ul>
          </div>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-book"></i> Penjualan (Orderan)
                  <?php $jmlh = $this->db->query("SELECT * FROM rb_penjualan where penjualan_dibaca='N'")->num_rows(); ?>
                  <span class="label label-success"><?php echo $jmlh; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Kamu punya <?php echo $jmlh; ?> orderan baru</li>
                  <li>
                    <ul class="menu">
                      <?php
                      //cek foto konsumen
                        $penjualan = $this->model_main->penjualan_baru(10);
                        foreach ($penjualan->result_array() as $row) {
                          if (trim($row['foto'])==''){$foto_user = 'blank.png';} else {$foto_user = $row['foto']; }
                          $pembeli = substr($row['nama_lengkap'],0,30);
                          $waktukirim = cek_terakhir($row['waktu_transaksi']);
                          if ($row['penjualan_dibaca']=='N'){ $color = '#f4f4f4'; }else{ $color = '#fff'; }
                          echo "<li style='background-color:$color'>
                                  <a href='".base_url()."administrator/detail_penjualan/$row[kode_transaksi]'>
                                    <div class='pull-left'>
                                    <img src='".base_url()."assets/foto_user/$foto_user' class='img-circle' alt='Image User'>
                                    </div>
                                    <h4>$row[kode_transaksi]<small><i class='fa fa-clock-o'></i> $waktukirim</small></h4>
                                    <p>Pembeli : $pembeli </p>
                                  </a>
                                </li>";
                        }

                      ?>
                    </ul>
                  </li>
                  <li class="footer"><a href="<?php echo base_url() ?>administrator/orders">Lihat semua orderan</a></li>
                </ul>
              </li>

            </ul>
          </div>

        </nav>
