        <section class="sidebar">
          <!-- Sidebar user panel -->
          <?php
          $log = $this->model_users->users_edit($this->session->username)->row_array();
          if ($log['foto']==''){ $foto = 'users.gif'; }else{ $foto = $log['foto']; }
          echo "<div class='user-panel'>
          <div class='pull-left image'>
          <img src='".base_url()."assets/foto_user/$foto' class='img-circle' alt='User Image'>
          </div>
          <div class='pull-left info'>
          <p>$log[nama_lengkap]</p>
          <a href=''><i class='fa fa-circle text-success'></i> Online</a>
          </div>
          </div>";
          ?>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header" style='color:#fff; text-transform:uppercase; border-bottom:2px solid #00c0ef'>MENU ADMIN</li>
            <li><a href="<?php echo base_url(); ?>administrator/home"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-th-list"></i> <span>Menu Utama</span><i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <?php

                if($this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/identitaswebsite'><i class='fa fa-circle-o'></i> Identitas Website</a></li>";
                }

                if($this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/menuwebsite'><i class='fa fa-circle-o'></i> Menu Website</a></li>";
                }

                if($this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/halamanbaru'><i class='fa fa-circle-o'></i> Halaman Baru</a></li>";
                }

                if($cek==1 OR $this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/imagesslider'><i class='fa fa-circle-o'></i> Images Slider</a></li>";
                }
                ?>
              </ul>
            </li>

            <li class="treeview">
              <a href="#"><i class="fa fa-gears"></i> <span>Modul System</span><i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <?php
                if($cek==1 OR $this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/kategori_produk'><i class='fa fa-circle-o'></i> Kategori Produk</a></li>";
                }

                if($cek==1 OR $this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/produk'><i class='fa fa-circle-o'></i> Produk</a></li>";
                }

                if($cek==1 OR $this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/rekening'><i class='fa fa-circle-o'></i> No Rekening</a></li>";
                }

                if($cek==1 OR $this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/konsumen'><i class='fa fa-circle-o'></i> Konsumen</a></li>";
                }

                    // if($cek==1 OR $this->session->level=='admin'){
                    //   echo "<li><a href='".base_url()."administrator/supplier'><i class='fa fa-circle-o'></i> Supplier</a></li>";
                    // }


                if($cek==1 OR $this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/orders'><i class='fa fa-circle-o'></i> Penjualan (Orderan)</a></li>";
                }

                if($cek==1 OR $this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/konfirmasi'><i class='fa fa-circle-o'></i> Konfirmasi Bayar</a></li>";
                }

                    // if($cek==1 OR $this->session->level=='admin'){
                    //   echo "<li><a href='".base_url()."administrator/pembelian'><i class='fa fa-circle-o'></i> Pembelian (Stok)</a></li>";
                    // }
                ?>
              </ul>
            </li>

            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-object-align-left"></i> <span>Modul Web</span><i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <?php

                if($cek==1 OR $this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/logowebsite'><i class='fa fa-circle-o'></i> Logo Website</a></li>";
                }

                if($cek==1 OR $this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/pesanmasuk'><i class='fa fa-circle-o'></i> Pesan Masuk</a></li>";
                }

                if($cek==1 OR $this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/testimoni'><i class='fa fa-circle-o'></i> <span>Testimoni</span></a></li>";
                }

                ?>
              </ul>
            </li>

            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-object-align-left"></i> <span>Modul Iklan</span><i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <?php

                if($cek==1 OR $this->session->level=='admin'){
                  echo "<li><a href='".base_url()."administrator/iklansidebar'><i class='fa fa-circle-o'></i> Iklan Sidebar</a></li>";
                }

                ?>
              </ul>
            </li>

            <!-- <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-object-align-left"></i> <span>Laporan Penjualan</span><i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
              <?php

              if($cek==1 OR $this->session->level=='admin'){
                  // echo "<li><a href='".base_url()."administrator/laporan_harian'><i class='fa fa-circle-o'></i> Laporan Harian</a></li>";
                  // echo "<li><a href='".base_url()."administrator/laporan_mingguan'><i class='fa fa-circle-o'></i> Laporan Mingguan</a></li>";
                  // echo "<li><a href='".base_url()."administrator/laporan_bulanan'><i class='fa fa-circle-o'></i> Laporan Bulanan</a></li>";
                  // echo "<li><a href='".base_url()."administrator/laporan_tahunan'><i class='fa fa-circle-o'></i> Laporan Tahunan</a></li>";
                }

              ?>
              </ul>
            </li> -->


            <!--  -->
            <li><a href="<?php echo base_url(); ?>administrator/edit_manajemenuser/<?php echo $this->session->username; ?>"><i class="fa fa-user"></i> <span>Edit Profile</span></a></li>
            <li><a href="<?php echo base_url(); ?>administrator/logout"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>
          </ul>
        </section>
