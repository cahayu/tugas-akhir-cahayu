<?php
    echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Edit Produk Terpilih</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart('administrator/edit_produk',$attributes);

          echo "<div class='col-md-12'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <input type='hidden' name='id' value='$rows[id_produk]'>
                    <tr><th scope='row'>Kategori</th>                   <td><select name='a' class='form-control' required>
                                                                            <option value='' selected>- Pilih Kategori Produk -</option>";
                                                                            foreach ($record as $row){
                                                                              if ($rows['id_kategori_produk']==$row['id_kategori_produk']){
                                                                                echo "<option value='$row[id_kategori_produk]' selected>$row[nama_kategori]</option>";
                                                                              }else{
                                                                                echo "<option value='$row[id_kategori_produk]'>$row[nama_kategori]</option>";
                                                                              }
                                                                            }



                    echo "</td></tr>
                    <tr><th width='130px' scope='row'>Nama Produk</th>  <td><input type='text' class='form-control' name='b' value='$rows[nama_produk]' placeholder='Nama Produk' required></td></tr>
                    <tr><th scope='row'>Satuan</th>                     <td><input type='text' class='form-control' name='c' value='$rows[satuan]' placeholder='pcs, kg, dus' required></td></tr>
                    <tr><th scope='row'>Berat</th>                      <td><input type='text' class='form-control' name='berat' value='$rows[berat]' placeholder='gram' required></td></tr>
                    <tr><th scope='row'>Stok</th>                      <td><input type='number' min='1' max='10' class='form-control' name='stok' value='$rows[stok]' placeholder='10' required></td></tr>"
                    ;
                    // <tr><th scope='row'>Harga Beli</th>                 <td><input type='number' class='form-control' name='d' value='$rows[harga_beli]' placeholder='1000' required></td></tr>
                    echo "
                    <tr><th scope='row'>Harga Konsumen</th>             <td><input type='number' class='form-control' name='f' value='$rows[harga_konsumen]' placeholder='1000' required></td></tr>
                    <tr><th scope='row'>Keterangan</th>                 <td><textarea  id='editor1' class='form-control' name='ff'>$rows[keterangan]</textarea></td></tr>
                    <tr><th scope='row'>Gambar</th>                     <td><input type='file' class='form-control' name='g'><hr style='margin:5px'>";
                                                                    if (trim($rows['gambar'])==''){$gambar_produk = 'no-image.png'; }else{$gambar_produk = $rows['gambar']; }
                                                                    echo "<img class='img-thumbnail' style='height:20%' src='".base_url()."assets/foto_produk/$gambar_produk' width='50%' alt='no image'></td></tr>";


                    echo "</td></tr>
                  </tbody>
                  </table>
                </div>
              </div>
              <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Update</button>
                    <a href='".base_url()."administrator/produk'><button type='button' class='btn btn-default pull-right'>Kembali</button></a>

                  </div>
            </div>";
