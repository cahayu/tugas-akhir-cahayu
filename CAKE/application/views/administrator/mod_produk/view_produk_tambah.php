<?php
    echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Tambah Produk Baru</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart('administrator/tambah_produk',$attributes);
          echo "<div class='col-md-12'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <input type='hidden' name='id' value=''>
                    <tr><th scope='row'>Kategori</th>                   <td><select name='a' class='form-control' required>
                                                                            <option value='' selected>- Pilih Kategori Produk -</option>";
                                                                            foreach ($record as $row){
                                                                                echo "<option value='$row[id_kategori_produk]'>$row[nama_kategori]</option>";
                                                                            }
                    echo "</td></tr>
                    <tr><th width='130px' scope='row'>Nama Produk</th>  <td><input type='text' class='form-control' name='b' placeholder='Nama Produk' required></td></tr>
                    <tr><th scope='row'>Satuan</th>                     <td><input type='text' class='form-control' name='c' placeholder='pcs, kg, dus' required></td></tr>
                    <tr><th scope='row'>Berat</th>                      <td><input type='text' class='form-control' name='berat' placeholder='gram' required></td></tr>
                    <tr><th scope='row'>Stok</th>                      <td><input type='number' min='1' max='10' class='form-control' name='stok' placeholder='10' required></td></tr>";
                    // <tr><th scope='row'>Harga Beli</th>                 <td><input type='number' class='form-control' name='d' placeholder='1000' required></td></tr>
                    echo "
                    <tr><th scope='row'>Harga Konsumen</th>             <td><input type='number' class='form-control' name='f' placeholder='1000' required></td></tr>
                    <tr><th scope='row'>Keterangan</th>                 <td><textarea id='editor1' class='form-control' name='ff'></textarea></td></tr>
                    <tr><th scope='row'>Gambar</th>                     <td><input type='file' class='form-control' name='g'></td></tr>
                  </tbody>
                  </table>
                </div>
              </div>
              <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Tambahkan</button>
                    <a href='".base_url()."administrator/produk'><button type='button' class='btn btn-default pull-right'>Kembali</button></a>

                  </div>
            </div>";
