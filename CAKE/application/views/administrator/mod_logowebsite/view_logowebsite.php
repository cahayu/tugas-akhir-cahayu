            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Ganti Logo Website</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example" class="table table-bordered table-striped">
                    <tbody>
                    <?php
                    $attributes = array('class'=>'form-horizontal','role'=>'form');
                    echo form_open_multipart('administrator/logowebsite/',$attributes);
                    $no = 1;
                    foreach ($record->result_array() as $row){
                    echo "<input type='hidden' name='id' value='$row[id_logo]'>
                          <tr><td width=120px>Logo Terpasang</td><td><a href=''><img class='img-thumbnail' style='height:20%' src='".base_url()."assets/logo_web/$row[gambar]' alt='no image'></a></td></tr>
                          <tr><td>Ganti Logo</td><td><input type='file' name='logo' class='form-control'></td></tr>";
                      $no++;
                    }
                    ?>
                  </tbody>
                </table>

                <div class='box-footer'>
                <button type='submit' name='submit' class='btn btn-info'>Update</button>
                <a href='home'><button type='button' class='btn btn-default pull-right'>Kembali</button></a>
                </div>
                <?
                echo form_close();
              ?>
              </div>
